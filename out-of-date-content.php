<?php
/**
 * Plugin Name: Out of Date Content
 * Plugin URI: https://vectorandink.com/
 * Description: Show message on posts that have reached a chosen staleness
 * Version: 0.1
 * Author: Daniel Braun
 * Author URI: https://vectorandink.com
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OutOfDateContent {

	public function __construct() {
		if ( is_admin() ) {
			add_action( 'load-post.php', array( $this, 'init_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );

			add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts' ) );
		}
	}

	/**
	 * Load scripts / styles
	 */
	public function load_scripts() {
		wp_enqueue_style( 'out-of-date-content-style', plugin_dir_url( __FILE__ ) . 'assets/style.css', '', '1.0.0', 'all' );
	}

	/**
	 * Meta box initialization.
	 */
	public function init_metabox() {
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
		add_action( 'save_post', array( $this, 'save_metabox' ), 10, 2 );
	}

	/**
	 * Adds the meta box.
	 */
	public function add_metabox() {
		add_meta_box(
			'days-before-outdated',
			'Days Before Outdated',
			array( $this, 'render_metabox' ),
			'post',
			'side',
			'low'
		);

	}

	/**
	 * Renders the meta box.
	 * @var $post WP_Post
	 */
	public function render_metabox( $post ) {
		// Add nonce for security and authentication.
		$post_id            = $post->ID;
		$publish_date       = new DateTime( $post->post_date );
		$publish_date_check = new DateTime( $post->post_date );

		$days_saved = get_post_meta( $post_id, 'expiry_days', true );
		$days       = 365;

		if ( $days_saved ) :
			$days = $days_saved;
		endif;

		$never_expire_saved = get_post_meta( $post_id, 'never_expire', true );
		$never_expire       = "on";

		if ( $never_expire_saved === null ): $never_expire = "off"; endif;

		$days_interval = new DateInterval( 'P' . $days . 'D' );
		wp_nonce_field( 'content_out_of_date_action', 'out_of_date_' . $post_id );
		echo '<div class="out-of-date-content-form">';
		echo '<label>Number of days until stale:';

		if ( $never_expire == "on" ):
			echo '<br><input min="1" max="9999" disabled class="days" type="number" value="' . $days . '" name="days">';
		else:
			echo '<br><input min="1" max="9999" class="days" type="number" value="' . $days . '" name="days">';
			if ( $publish_date_check->add( $days_interval ) > new DateTime( 'now' ) ) :
				echo ' Goes stale on: ' . $this->get_expiry_date( $publish_date, $days_interval ) . '<br>';
			else:
				echo ' <span class="warning">Went stale on: ' . $this->get_expiry_date( $publish_date, $days_interval ) . '</span><br>';
			endif;
		endif;
		echo '</label>';

		echo '</div><div>';
		echo '<label>Never go stale: ';
		if ( $never_expire == "on" ):
			echo '<input name="never_expire_checkbox" type="checkbox" checked>';
		else:
			echo '<input name="never_expire_checkbox" type="checkbox">';
		endif;
		echo '</label></div>';
	}

	/**
	 * Handles saving the meta box.
	 *
	 * @param int $post_id Post ID.
	 * @param WP_Post $post Post object.
	 *
	 * @return null
	 */
	public function save_metabox( $post_id, $post ) {

		// Add nonce for security and authentication.
		$nonce_name   = isset( $_POST[ 'out_of_date_' . $post_id ] ) ? $_POST[ 'out_of_date_' . $post_id ] : '';
		$nonce_action = 'content_out_of_date_action';

		$days         = $_POST['days'];
		$never_expire = $_POST['never_expire_checkbox'];

		// Check if nonce is set.
		if ( ! isset( $nonce_name ) ) {
			return;
		}

		// Check if nonce is valid.
		if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
			return;
		}

		// Check if user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Check if not an autosave.
		if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		// Check if not a revision.
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		$update_result = update_post_meta( $post_id, 'never_expire', $never_expire );
		if ( ! $days ) {
			$days = 365;
		}
		$update_result = update_post_meta( $post_id, 'expiry_days', $days );

	}

	/**
	 * @param $publish_date DateTime
	 * @param $days_to_stale DateInterval
	 *
	 * @return string
	 */
	public function get_expiry_date( $publish_date, $days_to_stale ) {
		$expiry_date = date_format( $publish_date->add( $days_to_stale ), 'M j, Y' );

		return $expiry_date;
	}
}

new OutOfDateContent();