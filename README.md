# Out Of Date Content

Use this plugin to notify website visitors that a blog post has become stale, or out of date.

New metaboxes are added to blog posts that allow you to choose the number of days before a post should indicate to the reader that it is out of date. You may chose that a post never goes out of date.
